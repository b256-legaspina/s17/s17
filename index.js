// console.log("Hello World!");






/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo () {
	let fullName = prompt("What is your name?");
	console.log("Hello, " + fullName);

	let agePrompt = prompt("How old are you?");
	console.log("You are " + agePrompt + " years old");

	let locPrompt = prompt("Where do you leave?");
	console.log("You live in " + locPrompt);

	alert("Thank you for answering!")
	};

	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topBands() { 
		let topBand1 = "The Beatles";
		let topBand2 = "U2";
		let topBand3 = "The Who";
		let topBand4 = "The Rolling Stones";
		let topBand5 = "Nirvana";

	console.log("1. " + topBand1);	
	console.log("2. " + topBand2);	
	console.log("3. " + topBand3);	
	console.log("4. " + topBand4);	
	console.log("5. " + topBand5);	
	};

	topBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topMovies() {
		let topMovie1 = "Cocaine Bear";
		let topMovie2 = "All That Breathes";
		let topMovie3 = "Knock at the Cabin";
		let topMovie4 = "Jesus Revolution";
		let topMovie5 = "Sharper";

		let cocaineBear = "69%";
		let allThatBreathes = "99%";
		let knockCabin = "67%";
		let jesusRevolution = "60%";
		let sharper = "69%";

	console.log("1. " + topMovie1);
	console.log("Rotten Tomatoes Rating: " + cocaineBear);
	console.log("2. " + topMovie2);
	console.log("Rotten Tomatoes Rating: " + allThatBreathes);
	console.log("3. " + topMovie3);
	console.log("Rotten Tomatoes Rating: " + knockCabin);
	console.log("4. " + topMovie4);
	console.log("Rotten Tomatoes Rating: " + jesusRevolution);
	console.log("5. " + topMovie5);
	console.log("Rotten Tomatoes Rating: " + sharper);

	};

	topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


/*let printFriends =*/ function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

};
printUsers();


// console.log(friend1);
// console.log(friend2);